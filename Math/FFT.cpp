const double PI = acos(-1);
typedef complex<double> base;
void fft (vector<base> & a, bool invert) {
	int n = (int) a.size();
	for (int i=1, j=0; i<n; ++i) {
		int bit = n >> 1;
		for (; j>=bit; bit>>=1)
			j -= bit;
		j += bit;
		if (i < j)
			swap (a[i], a[j]);
	}
	for (int len=2; len<=n; len<<=1) {
		double ang = 2*PI/len * (invert ? -1 : 1);
		base wlen (cos(ang), sin(ang));
		for (int i=0; i<n; i+=len) {
			base w (1);
			for (int j=0; j<len/2; ++j) {
				base u = a[i+j],  v = a[i+j+len/2] * w;
				a[i+j] = u + v;
				a[i+j+len/2] = u - v;
				w *= wlen;
			}
		}
	}
	if (invert)
		for (int i=0; i<n; ++i)
			a[i] /= n;
}
 
void multiply (const vector<int> &a, const vector<int> &b, vector<int> &res){
	vector<base> fa (a.begin(), a.end()),  fb (b.begin(), b.end());
	size_t n = 1;
	while (n < max (a.size(), b.size()))  n <<= 1;
	n <<= 1;
	fa.resize (n),  fb.resize (n);
	fft (fa, false),  fft (fb, false);
	for (size_t i=0; i<n; ++i)
		fa[i] *= fb[i];
	fft (fa, true);
	res.resize (n);
	for (size_t i=0; i<n; ++i)
		res[i] = int (fa[i].real() + 0.5);
}

// NTT?

const int mod = 7340033; // c * 2^k + 1 
const ll root = 5; // root = g ^ c % mod 
const ll root_1 = 4404020; root_l = (root)^-1 % mod
const ll root_pw = 1<<20; root_pw = (1 << k)
int rev_element[7340033];

ll getmod(ll a, ll tmod) {return ((a%tmod)+tmod)%tmod;}

void fft (vector<ll> & a, bool invert) {
  int n = (int) a.size();
  for (int i=1, j=0; i<n; ++i) {
    int bit = n >> 1;
    for (; j>=bit; bit>>=1)
      j -= bit;
    j += bit;
    if (i < j)
      swap (a[i], a[j]);
  }
  for (int len=2; len<=n; len<<=1) {
    ll wlen = invert ? root_1 : root;
    for (int i=len; i<root_pw; i<<=1)
      wlen = ll (wlen * 1ll * wlen % mod);
    for (int i=0; i<n; i+=len) {
      ll w = 1;
      for (int j=0; j<len/2; ++j) {
        ll u = a[i+j],  v = ll (a[i+j+len/2] * 1ll * w % mod);
        a[i+j] = getmod(u+v,mod);
        a[i+j+len/2] = getmod(u-v,mod);
        w = ll (w * 1ll * wlen % mod);
      }
    }
  }
  if (invert) {
    ll nrev = rev_element[n];
    for (int i=0; i<n; ++i)
      a[i] = int (a[i] * 1ll * nrev % mod);
  }
}

void precalc(){
    rev_element[1] = 1;
    for (int i=2; i<mod; i++)
      rev_element[i] = (mod - (mod/i) * rev_element[mod%i] % mod) % mod;
}

void multiply (const vector<ll> & a, const vector<ll> & b, vector<ll> & res) {
    vector <ll> fa (a.begin(), a.end()),  fb (b.begin(), b.end());
    size_t n = 1;
    while (n < max (a.size(), b.size()))  n <<= 1;
    n <<= 1;
    fa.resize (n),  fb.resize (n);
    fft (fa, false),  fft (fb, false);
    forn(i,n)
      fa[i] *= fb[i];
    fft (fa, true);
    res.resize (n);
    forn(i,n) // for(i=0;i<n;)
      res[i] = fa[i] % mod;
}
