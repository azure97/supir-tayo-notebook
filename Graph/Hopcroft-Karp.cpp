// assume that leftSide is numbered [1..n], rightSide [1..m]
// leftMatch[i] -> i-th left matched with who on the rightSide
// rightMatch[i] -> opposite
// for indexing in flag and mvc/mis, rightSide +n (flag size x2!)
void bfs() {
  queue<int> q;
  for(int i = 1 ; i <= n ; i++)
    if(leftMatch[i] == -1) {
      dist[i] = 0;
      q.push(i);
    }
    else
      dist[i] = INF;
  
  while(!q.empty()) {
    int cur = q.front();
    q.pop();

    for(int nex : adj[cur]) {
      if(rightMatch[nex] != -1 && dist[rightMatch[nex]] == INF) {
        dist[rightMatch[nex]] = dist[cur] + 1;
        q.push(rightMatch[nex]);
      }
    }
  }    
}

int augment(int now) {
  if (vis[now]) return 0;
  vis[now] = 1;

  for (int nex : adj[now]) {
    if (rightMatch[nex] == -1 || (dist[rightMatch[nex]] == dist[now] + 1 && augment(rightMatch[nex]))) {
      rightMatch[nex] = now;
      leftMatch[now] = nex;
      return 1;
    }
  }

  return 0;
}

int hopcroftKarp() {
  int ret = 0;
  memset(leftMatch, -1, sizeof leftMatch);
  memset(rightMatch, -1, sizeof rightMatch);
  
  while (1) {
    bfs();
    memset(vis, 0, sizeof vis);

    int add = 0;
    for (int i = 1; i <= n; i++)
      if (leftMatch[i] == -1) {
        add += augment(i);
      }
    
    if (add == 0) {
      break;
    }
    ret += add;
  }
  
  return ret;
}

void dfsMark(int now) {
  if (now == -1 || flag[now]) return;
  flag[now] = true;

  for (int nex : adj[now]) {
    if (rightMatch[nex] != now) {
      flag[n+nex] = 1;
      dfsMark(rightMatch[nex]);
    }
  }
}

void konigs() {
  memset(flag, 0, sizeof flag);
  for (int i = 1 ; i <= n ; i++)
    if (leftMatch[i] == -1) {
      dfsMark(i);
    }
  
  vector<int> mvc, mis;
  // leftSide
  for (int i = 1 ; i <= n ; i++) {
    if (!flag[i]) mvc.push_back(i);
    else mis.push_back(i);
  }
  // rightSide
  for (int i = 1 ; i <= m ; i++) {
    if (flag[n+i]) mvc.push_back(n+i);
    else mis.push_back(n+i);
  }

  // now do whatever you want with mvc or mis
}
