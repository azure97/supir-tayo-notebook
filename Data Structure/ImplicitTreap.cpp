#define ll long long 
// See random number section.
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

class ImpTreap{ // This Treap is ONE based
	struct Node{
		unsigned int prior;
		int key, siz;
		bool lazy;
		ll sum;
		Node *l, *r;
		Node(){}
		Node(unsigned int _prior, int _key){
			prior = _prior; key = _key; siz = 1; lazy = 0;
			sum = key;
			l = r = NULL;
		}
	};
	typedef Node * pnode;
	int cnt(pnode t){
		return (!t) ? 0 : t->siz;
	}
	int sum(pnode t){
		return (!t) ? 0 : t->sum;
	}
	void upd_siz(pnode t){
		if(t){
			t->siz = 1 + cnt(t->l) + cnt(t->r);
			t->sum = t->key + sum(t->l) + sum(t->r);
		}
	}
	pnode root = NULL;

	void propagate(pnode &t){
		if(t && t->lazy){
			t->lazy = false;
			swap(t->l, t->r);
			if(t->l) t->l->lazy ^= 1;
			if(t->r) t->r->lazy ^= 1;
		}
	}
	void print(pnode t){
		propagate(t);
		if(!t) return;
		print(t->l); printf("(%d)", t->key); print(t->r);
	}
	int getKey(pnode t, int idx, int add = 0){
		if(!t) return -1;
		else{
			int curIdx = 1 + add + cnt(t->l);
			if(curIdx == idx) return t->key;
			return (curIdx > idx) ? getKey(t->l, idx, add) : getKey(t->r, idx, curIdx);
		}
	}

	public:
		ImpTreap(){}
		void split(pnode t, pnode &l, pnode &r, int idx, int add = 0){// Split to [1..idx-1], [idx, N]
			if(!t) l = r = NULL;
			else{
				propagate(t);
				int curIdx = 1 + add + cnt(t->l);
				if(curIdx >= idx){ split(t->l, l, t->l, idx, add); r = t; }
				else{ split(t->r, t->r, r, idx, curIdx); l = t; }
			}
			upd_siz(t);
		}
		void merge(pnode &t, pnode l, pnode r){
			if(!l || !r){
				t = (l) ? l : r;
			}else{
				propagate(l); propagate(r);
				if(l->prior > r->prior){ merge(l->r, l->r, r); t = l; }
				else{ merge(r->l, l, r->l); t = r; }
			}
			upd_siz(t);
		}
		void insert(int key, int idx){
			pnode tmp = new Node(rng(), key);
			pnode t1, t2, t3;
			split(root, t1, t2, idx); //t1 = [1..idx-1]
			merge(t3, t1, tmp);
			merge(root, t3, t2);
		}
		void del(int idx){
			pnode t1, t2, t3, tmp;
			split(root, t1, t2, idx);
			split(t2, tmp, t3, idx+1);
			merge(root, t1, t3);
		}
		void reverse(int l, int r){ // Reverse the interval [l, r] inclusive
			pnode t1, t2, t3;
			split(root, t1, t2, l);
			split(t2, t2, t3, r-l+2);
			t2->lazy ^= 1; propagate(t2);
			merge(t2, t2, t3);
			merge(root, t1, t2);
		}
		int getKey(int idx){
			return getKey(root, idx);
		}
		ll getSum(int l, int r){
			ll tmpSum = 0;
			pnode t1, t2, t3;
			split(root, t1, t2, l);
			split(t2, t2, t3, r-l+2);
			tmpSum = sum(t2);
			merge(t2, t2, t3);
			merge(root, t1, t2);
			return tmpSum;
		}
		void print(){print(root);}
};