ll bit[2][N]; 
void update(int a, int b, ll val) {
  update(bit[0], a, val);
  update(bit[0], b + 1, -val);
  update(bit[1], a, val * (a - 1));
  update(bit[1], b + 1, -val * b);
}
ll query(int x) {
  return query(bit[0], x) * x - query(bit[1], x);
}
